# TP 2

## II.

| Name   | IP        | MAC               | Fonction                   |
| ------ | --------- | ----------------- | -------------------------- |
| enp0s3 | 10.0.2.15 | 08:00:27:7f:07:55 | Carte NAT (accès internet) |
| enp0s8 | 10.2.1.3  | 08:00:27:24:5d:2a | Carte Ethernet             |

### 4

- ![screen swap ip](reseau.PNG)

- ![screen ping](reseau2.PNG)

### 5

SS :

```
[arthur@localhost ~]$ ss -ltunp
Netid State      Recv-Q Send-Q        Local Address:Port                       Peer Address:Port
udp   UNCONN     0      0                         *:68                                    *:*
udp   UNCONN     0      0                         *:68                                    *:*
tcp   LISTEN     0      100               127.0.0.1:25                                    *:*
tcp   LISTEN     0      128                       *:22                                  *:*
tcp   LISTEN     0      100                   [::1]:25                                 [::]:*
tcp   LISTEN     0      128                    [::]:22                            [::]:*
[arthur@localhost ~]$
```

### II.

### SSH

1. Le ssh écoute sur le port 22

```
[arthur@localhost ~]$ ss -ltunp
Netid State      Recv-Q Send-Q        Local Address:Port                       Peer Address:Port                                  *:*
tcp   LISTEN     0      128                       *:22                                 *:*
tcp   LISTEN     0      128                    [::]:22                               [::]:*
[arthur@localhost ~]$
```

2.

```
PS C:\Users\arthu> ssh arthur@10.2.1.3
```

### Firewall

On voit que c'est sur le port 22.

```
[arthur@localhost ssh]$ sudo cat sshd_config
#       $OpenBSD: sshd_config,v 1.100 2016/08/15 12:32:04 naddy Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 22
```

On change le port de 22 à 2000.

```
[arthur@localhost ssh]$ sudo nano sshd_config
#       $OpenBSD: sshd_config,v 1.100 2016/08/15 12:32:04 naddy Exp $

# This is the sshd server system-wide configuration file.  See
# sshd_config(5) for more information.

# This sshd was compiled with PATH=/usr/local/bin:/usr/bin

# The strategy used for options in the default sshd_config shipped with
# OpenSSH is to specify options with their default value where
# possible, but leave them commented.  Uncommented options override the
# default value.

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
#Port 2000.
```

On test la connections est évidemmeent ça ne marche pas puisque le firewall fait son travaille.

On ajoute notre port au ports acceptés par le firewall.

```
sudo firewall-cmd --add-port=2000/tcp --permanent
```

B. Netcat

```
[arthur@localhost ~]$ nc -l -p 3000
a
hello
cool
```

```
PS C:\Users\arthu\Downloads\netcat-1.11> .\nc 10.2.1.3 3000                                                    a
hello
cool
```

On peut voir la connection nc en dessous de la connection ssh

```
PS C:\Users\arthu> netstat

Connexions actives

  Proto  Adresse locale         Adresse distante       État
  TCP    10.2.1.1:49999         10.2.1.3:2000          ESTABLISHED
  TCP    10.2.1.1:51178         10.2.1.3:3000          ESTABLISHED
  TCP    192.168.0.29:49679     ams10-012:http         ESTABLISHED
  TCP    192.168.0.29:49691     40.67.251.132:https    ESTABLISHED
  TCP    192.168.0.29:49774     162.159.136.234:https  ESTABLISHED
  TCP    192.168.0.29:49865     wb-in-f188:5228        ESTABLISHED
  TCP    192.168.0.29:51107     52.157.234.37:https    ESTABLISHED
  TCP    192.168.0.29:51147     ec2-3-218-125-188:https  ESTABLISHED
  TCP    192.168.0.29:51148     ec2-3-218-125-188:https  ESTABLISHED
  TCP    192.168.0.29:51166     par21s04-in-f163:https  ESTABLISHED
  TCP    192.168.0.29:51168     162.159.135.233:https  ESTABLISHED
  TCP    192.168.0.29:51175     lon02-024:http         TIME_WAIT
  TCP    192.168.0.29:51183     152.199.23.154:http    TIME_WAIT
  TCP    192.168.0.29:51184     152.199.23.154:http    TIME_WAIT
  TCP    192.168.0.29:51187     52.142.21.136:https    TIME_WAIT
  TCP    192.168.0.29:51188     65.52.108.90:https     TIME_WAIT
  TCP    192.168.0.29:51191     152.199.23.154:http    TIME_WAIT
  TCP    192.168.0.29:51192     152.199.23.154:https   ESTABLISHED
  TCP    192.168.0.29:51193     a2-18-245-67:http      TIME_WAIT
  TCP    192.168.0.29:51194     152.199.23.154:http    TIME_WAIT
  TCP    192.168.0.29:51195     52.114.36.2:https      TIME_WAIT
  TCP    [::1]:27275            LAPTOP-T6GHI3P2:51205  TIME_WAIT
  TCP    [::1]:27275            LAPTOP-T6GHI3P2:51206  TIME_WAIT
  TCP    [::1]:27275            LAPTOP-T6GHI3P2:51209  TIME_WAIT
PS C:\Users\arthu>
```

## III. Routage Statique

A. PC1

```
[arthur@localhost ~]$ ip route add 10.2.1.0/24 via 10.2.1.1 dev enp0s8
```

B. PC2

```
PS C:\Users\arthu> ping 10.2.1.1

Envoi d’une requête 'Ping'  10.2.1.1 avec 32 octets de données :
Réponse de 10.2.1.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.1.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.1.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.1.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 10.2.1.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

C. VM1

```
[root@localhost ~]# ping 10.2.2.13
PING 10.2.2.13 (10.2.2.13) 56(84) bytes of data.
```

```
C:\WINDOWS\system32>ping 10.2.12.2

Envoi d’une requête 'Ping'  10.2.12.2 avec 32 octets de données :
Réponse de 10.2.12.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.12.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.12.1 : octets=32 temps<1ms TTL=128
Réponse de 10.2.12.1 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 10.2.12.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 2ms, Moyenne = 0ms
```

D.VM2

```
[root@localhost ~]# nc -l -p 80
```

```
[root@localhost ~]# nc vm2.tp2.b1 -p 22
```

```
C:\\WINDOWS\ping vm2.tp2.b1
```

```
C:\\WINDOWS\tracert vm2.tp2.b1
```
